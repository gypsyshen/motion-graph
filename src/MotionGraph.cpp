#include "MotionGraph.h"
#include "Bvh.h"

/** put translated joint vertices into array */

static bool compareOffsets(const OFFSET &offset_1, const OFFSET &offset_2) {

	if (offset_1.x == offset_2.x && offset_1.y == offset_2.y && offset_1.z == offset_2.z) {
		return true;
	}
	return false;
}



static bool compareJoints(const JOINT * joint_1, const JOINT * joint_2) {
	
	// compare offsets
/*	if (!compareOffsets(joint_1->offset, joint_2->offset)) {
		cout << "offset of " << joint_1->name << ": " << joint_1->offset.x << ", " << joint_1->offset.y
			<< ", " << joint_1->offset.z << endl;
		cout << "offset of " << joint_2->name << ": " << joint_2->offset.x << ", " << joint_2->offset.y
			<< ", " << joint_2->offset.z << endl;
		return false;		
	} */

	// compare channels
	int num_channel_1 = joint_1->num_channels;
	int num_channel_2 = joint_2->num_channels;
	if (num_channel_1 != num_channel_2) return false;

	// compare children
	int num_children_1 = joint_1->children.size();
	int num_children_2 = joint_2->children.size();

	if (num_children_1 != num_children_2) return false;

	for (int i = 0; i < num_children_1; ++i) {

		const JOINT * child_1 = joint_1->children.at(i);
		const JOINT * child_2 = joint_2->children.at(i);

		bool same = compareJoints(child_1, child_2);

		if (!same) return false;
	}

	return true;
}


static void translate(Bvh * bvh, Bvh * bvh2) {

	// translate every frame of bvh2 by some distance
	OFFSET t(0.f, 0.f, 60.f);

	bvh2->translateRootJoints(t);
}


static void compute_joint_params(JOINT * joint) {
	
	joint->velocity = VECTOR();
	joint->acceleration = VECTOR();
	joint->net_torque = VECTOR();
	joint->force = VECTOR();

	for (auto &child : joint->children) {
		compute_joint_properties(child);
	}
}

static float sum_joint_torques(JOINT * joint) {
	
	float torque = joint->torque;
	
	for (auto & child : joint->children) {
		torque += child->torque;
	}

	return torque;
}

// update the motion data of a joint in a certain frame
static void update_motion_data(JOINT * joint, MOTION &motion, int frame) {

	int start_index = frame * motion.num_motion_channels;
	int joint_start_index = start_index + joint->channel_start;

	// TODO: update the motion data of current joint

	// traversal the childeren
	for (auto & child : joint->children) {
		update_motion_data(child, motion, frame);
	}
}

// space time constraints
static void make_transition_timespace(Bvh * bvh_t1, Bvh * bvh_t2, MOTION & transitionMotion) {
	
	JOINT * rootJoint = (JOINT *)bvh_t1->getRootJoint();
	int num_frames = transitionMotion.num_frames;
	int num_motion_channels = transitionMotion.num_motion_channels;
	float sum_torque = 100000.f;
	float prev_sum_torque = 0.f;
	int iteration = 0;
	while (sum_torque > prev_sum_torque || iteration == 0) {
		prev_sum_torque = sum_torque;
		sum_torque = 0.f;
		for (int frame = 0; frame < num_frames; ++frame) {
			// Balafoutis:
			// In the first step velocities, accelerations, net torques, and forces at each joint are computed starting from the root node and working out to the tips of all the chains in the tree.
			compute_joint_params(rootJoint);

			// In the second step the joint torques are computed starting from the tips of the chains back to the root node.
			// TODO

			// Use the torques computed using Balafoutis to update gradient, pseudo-Hessian, and the step direction
			// TODO
			
			// Then update the motion data of this frame
			update_motion_data(rootJoint, transitionMotion, frame);

			// Sum up the torques
			sum_torque += sum_joint_torques(rootJoint);
		}
		iteration++;
	}
}

// linear interpolation
static void make_transition_linear(Bvh * bvh_t1, Bvh * bvh_t2, MOTION & transitionMotion) {

	bvh_t1->moveTo(bvh_t1->getNumFrames() - 1);
	bvh_t2->moveTo(0);

	const JOINT * rootJoint_t1 = bvh_t1->getRootJoint();

	OFFSET rootPos_t1 = bvh_t1->getRootPosition(bvh_t1->getNumFrames() - 1);
	OFFSET rootPos_t2 = bvh_t2->getRootPosition(0);

	// define how many frames to be generated between two motions
	int num_frames = transitionMotion.num_frames;
	int num_motion_channels = transitionMotion.num_motion_channels;

	const MOTION motion_t1 = bvh_t1->getMotion();
	const MOTION motion_t2 = bvh_t2->getMotion();

	// root joint start index
	for (int frame = 0; frame < num_frames; ++frame) 
	{
		int start_index = frame * num_motion_channels + rootJoint_t1->channel_start;
		for (int i = 0; i < rootJoint_t1->channel_start; ++i)
		{
			const short & channel = rootJoint_t1->channels_order[i];
			if (channel & Xposition) transitionMotion.data[start_index + i];
			if (channel & Yposition) transitionMotion.data[start_index + i];
			if (channel & Zposition) transitionMotion.data[start_index + i];
		}
	}

	// foreach frame interpolate and store floats
	for (int frame = 0; frame < num_frames; ++frame)
	{
		int rootjoint_start_index = frame * num_motion_channels + rootJoint_t1->channel_start;
		for (int i = 0; i < num_motion_channels; ++i)
		{
			// index of the current motion data
			int index = frame * num_motion_channels + i;

			// skip the rootjoint positions
			// assign the root joint rotation of the beginning of the 2nd motion to the ending of the 1st
			if (i >= rootJoint_t1->channel_start && i < rootJoint_t1->channel_start + rootJoint_t1->num_channels) {
				
				int k = i - rootJoint_t1->channel_start;
				const short & channel = rootJoint_t1->channels_order[k];
				if (channel & Xrotation) transitionMotion.data[index] = motion_t2.data[i];
				if (channel & Yrotation) transitionMotion.data[index] = motion_t2.data[i];
				if (channel & Zrotation) transitionMotion.data[index] = motion_t2.data[i];

				continue;
			}

			// interpolate motion data
			float x_t1 = motion_t1.data[num_motion_channels * (motion_t1.num_frames - 1) + i];
			float x_t2 = motion_t2.data[i];
			float x = x_t1 + frame * (x_t2 - x_t1) / num_frames;

			// update the transited data
			transitionMotion.data[index] = x;
		}
	}

}

// 3.2 Motion Transitions
static MOTION make_transition_xz(Bvh * bvh_t1, Bvh * bvh_t2, OFFSET v_t1, OFFSET v_t2, int num_frames, OFFSET &t) {
	
	bvh_t1->moveTo(bvh_t1->getNumFrames() - 1);
	bvh_t2->moveTo(0);

	const JOINT * rootJoint_t1 = bvh_t1->getRootJoint();

	OFFSET rootPos_t1 = bvh_t1->getRootPosition(bvh_t1->getNumFrames() - 1);
	OFFSET rootPos_t2 = bvh_t2->getRootPosition(0);

	// define how many frames to be generated between two motions
	MOTION transitionMotion;
	transitionMotion.num_frames = num_frames;
	int num_motion_channels = bvh_t1->getNumMotionChannels();
	transitionMotion.num_motion_channels = num_motion_channels;
	transitionMotion.data = new float[num_frames * num_motion_channels];

	const MOTION motion_t1 = bvh_t1->getMotion();
	const MOTION motion_t2 = bvh_t2->getMotion();

	// foreach frame interpolate and store floats
	for (int frame = 0; frame < num_frames; frame++)
	{
		int start_index = frame * num_motion_channels + rootJoint_t1->channel_start;

		for (int i = 0; i < rootJoint_t1->num_channels; ++i) {

			const short & channel = rootJoint_t1->channels_order[i];
			// linearly interpolate the y component of the transition motion
			if (channel & Yposition) {
				transitionMotion.data[start_index + i] = rootPos_t1.y + (rootPos_t2.y - rootPos_t1.y) * frame / num_frames;
			}
			// compute the x-z component
			if (channel & Xposition) {
				float value = 0.f;
				for (int alpha = 0; alpha <= frame; ++alpha) {
					float ratio = float(alpha) / float(num_frames);
					value += v_t1.x * (1.f - ratio) + v_t2.x * ratio;
				}
				transitionMotion.data[start_index + i] = rootPos_t1.x + value;
			}
			if (channel & Zposition) {
				float value = 0.f;
				for (int alpha = 0; alpha <= frame; ++alpha) {
					float ratio = float(alpha) / float(num_frames);
					value += v_t1.z * (1.f - ratio) + v_t2.z * ratio;
				}
				transitionMotion.data[start_index + i] = rootPos_t1.z + value;
			}
		}
	}

	// get the coordinate of the root joint of the ending of the translated motion
	{
		int frame = num_frames - 1;
		int start_index = frame * num_motion_channels + rootJoint_t1->channel_start;

		for (int i = 0; i < rootJoint_t1->num_channels; ++i) {

			const short & channel = rootJoint_t1->channels_order[i];
			if (channel & Yposition) t.y = transitionMotion.data[start_index + i];
			if (channel & Xposition) t.x = transitionMotion.data[start_index + i];
			if (channel & Zposition) t.z = transitionMotion.data[start_index + i];
		}
	}

	return transitionMotion;
}

void MotionGraph::bvh_to_vertices(const JOINT* joint,
	std::vector<vec4>& vertices,
	std::vector<unsigned short>&   indices,
	GLshort                 parentIndex)
{
	// vertex from current joint is in 4-th ROW (column-major ordering)
	vec4 translatedVertex = joint->matrix[3];

	// pushing current 
	vertices.push_back(translatedVertex);

	// avoid putting root twice
	GLshort myindex = vertices.size() - 1;
	if (parentIndex != myindex)
	{
		indices.push_back(parentIndex);
		indices.push_back(myindex);
	}

	// foreach child same thing
	for (auto& child : joint->children)
		bvh_to_vertices(child, vertices, indices, myindex);
}

void MotionGraph::bvh_load_upload(int frame) {

	// using Bvh class
	if (bvh == NULL)
	{
		bvh = new Bvh;
		bvh->load("02_09.bvh");

		bvh2 = new Bvh;
		bvh2->load("09_01.bvh");

		bool same = compareJoints(bvh->getRootJoint(), bvh2->getRootJoint());
		if (same) {
			cout << "02_01.bvh and 02_09.bvh are same." << endl;

			// translate
			OFFSET from, to;

			// linear interpolation with speed transition: v_t1: 0, 0, 0.2; v_t2: 0, 0, 0; transition frame: 20
			// make transition of the x-z plane
			OFFSET v_t1(0.f, 0.f, 0.f);
			OFFSET v_t2(0.f, 0.f, 0.2f);
			MOTION transitionMotion = make_transition_xz(bvh, bvh2, v_t1, v_t2, 20, to);

			// translate
			bvh2->moveTo(0);
			const JOINT * rootJoint2 = bvh2->getRootJoint();
			unsigned start_index_2 = rootJoint2->channel_start;
			for (unsigned i = 0; i < rootJoint2->num_channels; ++i) {
				float value = bvh2->getMotion().data[start_index_2 + i];
				const short& channel = rootJoint2->channels_order[i];
				if (channel & Xposition) from.x = value;
				if (channel & Yposition) from.y = value;
				if (channel & Zposition) from.z = value;
			}
			OFFSET t(to.x - from.x, to.y - from.y, to.z - from.z);

			bvh2->translateRootJoints(t);

			// linearly interpolate the motions
//			make_transition_linear(bvh, bvh2, transitionMotion);
			// TODO: try one iteration without checking the convergence of the objective function.
			// If the one iteration works, figure out how to check the convergence of the objective function
			make_transition_timespace(bvh, bvh2, transitionMotion);

			// concatenate motions
			bvh->concatenateMotion(transitionMotion);
			bvh->concatenateMotion(bvh2->getMotion());
		}
		else cout << "02_01.bvh and 02_09.bvh are different." << endl;		
	}

	int num_frame_bvh = bvh->getNumFrames();
	frame %= num_frame_bvh;
	bvh->moveTo(frame);
	const JOINT* rootJoint = bvh->getRootJoint();
	vertices.clear();
	indices.clear();
	bvh_to_vertices(rootJoint, vertices, indices);
	
	// here goes OpenGL stuff with gen/bind buffer and sending data
	// basically you want to use GL_DYNAMIC_DRAW so you can update same VBO

	// Load it into a VBO

	//	GLuint vertexbuffer;
//	GLuint vertexbuffer;
	glGenBuffers(1, &bvhVBO);
	glBindBuffer(GL_ARRAY_BUFFER, bvhVBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(vec4), &vertices[0], GL_DYNAMIC_DRAW);

	//	GLuint normalbuffer;
//	glGenBuffers(1, &normalbuffer);
//	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
//	glBufferData(GL_ARRAY_BUFFER, indexed_normals.size() * sizeof(glm::vec3), &indexed_normals[0], GL_STATIC_DRAW);

	// Generate a buffer for the indices as well
//	GLuint elementbuffer;
	glGenBuffers(1, &elementbuffer);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned short), &indices[0], GL_DYNAMIC_DRAW); 

}