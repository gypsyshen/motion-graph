/*
main file.
*/

// Include common headers
#include "Header.h"
#include "MotionGraph.h"
#include "Mesh.h"
#include "GLSLProgram.h"


// Global variables

GLFWwindow* window;

int window_size_w = 1024;
int window_size_h = 768;

// Entry point
int main(void) {

	// Initialise GLFW
	if (!glfwInit()) {

		fprintf(stderr, "Failed to initialize GLFW\n");
		return -1;
	}

	glfwWindowHint(GLFW_SAMPLES, 4);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

	// Open a window and create its OpenGL context
	window = glfwCreateWindow(window_size_w, window_size_h, "Motion Graph", NULL, NULL);
	if (window == NULL) {
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);

	// Initialize GLEW
	glewExperimental = true; // Needed for core profile
	if (glewInit() != GLEW_OK) {
		fprintf(stderr, "Failed to initialize GLEW\n");
		return -1;
	}

	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(window, GLFW_STICKY_KEYS, GL_TRUE);
	glfwSetCursorPos(window, window_size_w * 0.5f, window_size_h * 0.5f);

	// Dark blue background
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	// Enable depth test
	glEnable(GL_DEPTH_TEST);

	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	// Cull triangles which normal is not towards the camera
	glEnable(GL_CULL_FACE);

	// Draw the primitives in wire mode
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);

	// TODO: what's this?
	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);

	// ---------------- Initialize models ----------------
//	Mesh mesh_axis;
//	mesh_axis.LoadMesh("axis.obj");

	// ---------------- Initialize GLSL program ----------------
	GLSLProgram shader;
	shader.Init();

	// ---------------- Initialize motion capture data ----------------
	MotionGraph mocap;

	unsigned frame = 0;
	unsigned countFrame = 0;

	// ---------------------------------------------
	// main loop
	// ---------------------------------------------

	do{
/*		// --------------- Update ---------------
		if (countFrame >= 1) {
			countFrame = 0;
			frame++;
		}
		else {
			countFrame++;
		} // */
		mocap.bvh_load_upload( frame++ );

		computeSpotLightMatricesFromInputes();

		// --------------- Render to screen ---------------

		// Compute the MVP matrix from keyboard and mouse input
		computeMatricesFromInputs();
		glm::mat4 ProjectionMatrix = getProjectionMatrix();
		glm::mat4 ViewMatrix = getViewMatrix();		
		mat4 VP = ProjectionMatrix * ViewMatrix;
		glm::vec3 lightInvDir = vec3(0.f, 1.f, 1.f); // getLightInvDirection();

		shader.RenderToScreen(window_size_w, window_size_h, VP, ViewMatrix, lightInvDir, mocap);

		// Save current screen if ' ' is pressed
		saveScreenToImage();

		// Swap buffers
		glfwSwapBuffers(window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(window, GLFW_KEY_ESCAPE) != GLFW_PRESS &&
	glfwWindowShouldClose(window) == 0);

	// Close OpenGL window and terminate GLFW
	glfwTerminate();

	return 0;
}