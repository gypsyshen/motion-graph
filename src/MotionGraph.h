#ifndef MOTIONGRAPH_H
#define MOTIONGRAPH_H

#include "Header.h"
#include "Common.h"
#include "Bvh.h"

class MotionGraph {

// variables
private:
	vector<vec4> vertices;

	Bvh* bvh;
	Bvh* bvh2;
	
public:
	GLuint bvhVAO;
	GLuint bvhVBO;

//	GLuint vertexbuffer;
	GLuint elementbuffer;
	vector<unsigned short> indices;

// functions
private:

	void bvh_to_vertices(const JOINT* joint,
		vector<vec4>& vertices,
		vector<unsigned short>&   indices,
		GLshort                 parentIndex = 0);

public:

	void MotionGraph::bvh_load_upload(int frame = 1);


// constructor, destructor
public:

	MotionGraph() : bvh(NULL) {}

	~MotionGraph() {}
};

#endif