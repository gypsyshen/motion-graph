#ifndef JOINT_H
#define JOINT_H

//#include "Header.h"

struct Joint {
	float length;
	float localTheta;
	const Joint * child;

	Joint(const float &len, const float &t, const Joint * c) : length(len), localTheta(t), child(c) {}
};

#endif