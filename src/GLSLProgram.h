#ifndef GLSLPROGRAM_H
#define GLSLPROGRAM_H

#include "Header.h"
#include "Mesh.h"
#include "MotionGraph.h"

class GLSLProgram {
public:

	void Init();

	void RenderToScreen(int window_size_w, int window_size_h,
		const mat4 &VP, const mat4 &ViewMatrix,
		const vec3 &lightInvDir,
		const Mesh &mesh_axis) const;

	void RenderToScreen(int window_size_w, int window_size_h,
		const mat4 &VP, const mat4 &ViewMatrix,
		const vec3 &lightInvDir,
		const MotionGraph &mocap ) const;

	void CleanUp();

private:

	void DrawGeometry(const GLuint &vertexbuffer, const GLuint &normalbuffer, const GLuint &elementbuffer,
		const vector<unsigned short> &indices) const;

	void DrawGeometry(const GLuint &vertexbuffer, const GLuint &elementbuffer, 
		const vector<unsigned short> &indices) const;

	// IDs in shadow program
	GLuint programID;

	GLuint MatrixID;
	GLuint ViewMatrixID;
	GLuint ModelMatrixID;

	GLuint MaterialDiffuseID;
	GLuint MaterialAmbientID;
	GLuint MaterialSpecularID;

	GLuint lightInvDirID;
};

#endif