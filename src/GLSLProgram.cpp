/*
Implementation of class GLSLProgram.
*/

#include "GLSLProgram.h"
#include "Mesh.h"

void GLSLProgram::Init() {
	// Create and compile our GLSL program from the shaders
	programID = LoadShaders("BRDF.vertexshader", "BRDF.fragmentshader");

	// Get a handle for our "MVP" uniform
	MatrixID = glGetUniformLocation(programID, "MVP");
	ViewMatrixID = glGetUniformLocation(programID, "V");
	ModelMatrixID = glGetUniformLocation(programID, "M");

	// Get a handle for our "material" uniform
	MaterialDiffuseID = glGetUniformLocation(programID, "Material.diffuse");
	MaterialAmbientID = glGetUniformLocation(programID, "Material.ambient");
	MaterialSpecularID = glGetUniformLocation(programID, "Material.specular");

	// Get a handle for our "LightInvDirection_worldspace" uniform
	lightInvDirID = glGetUniformLocation(programID, "LightInvDirection_worldspace");
}

void GLSLProgram::DrawGeometry(const GLuint &vertexbuffer, const GLuint &elementbuffer,
	const vector<unsigned short> &indices) const {

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		4,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glLineWidth(3.f);
	glDrawElements(
		GL_LINES,
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(0);
}

void GLSLProgram::DrawGeometry(const GLuint &vertexbuffer, const GLuint &normalbuffer, const GLuint &elementbuffer,
	const vector<unsigned short> &indices) const {

	// 1rst attribute buffer : vertices
	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);

	// 2rd attribute buffer : normals
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, normalbuffer);
	glVertexAttribPointer(
		1,                                // attribute
		3,                                // size
		GL_FLOAT,                         // type
		GL_FALSE,                         // normalized?
		0,                                // stride
		(void*)0                          // array buffer offset
		);

	// Index buffer
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, elementbuffer);

	// Draw the triangles !
	glDrawElements(
		GL_TRIANGLES,      // mode
		indices.size(),    // count
		GL_UNSIGNED_SHORT, // type
		(void*)0           // element array buffer offset
		);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
}

// draw motion capture data
void GLSLProgram::RenderToScreen(int window_size_w, int window_size_h,
	const mat4 &VP, const mat4 &ViewMatrix,
	const vec3 &lightInvDir,
	const MotionGraph &mocap) const {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

	// draw the motion capture data
	glUniform3f(MaterialSpecularID, 1.f, 0.f, 0.f);
	glUniform3f(MaterialAmbientID, 1.f * 0.2f, 0.f, 0.f);
	glUniform3f(MaterialDiffuseID, 1.f, 0.f, 0.f);

	mat4 ModelMatrix = mat4(1.f); // rotate(-90.f, 0.f, 0.f, 1.f) * scale(0.5f, 0.5f, 0.5f);
	mat4 MVP = VP * ModelMatrix;

	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
	glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);


	DrawGeometry(mocap.bvhVBO, mocap.elementbuffer, mocap.indices);
}

void GLSLProgram::RenderToScreen(int window_size_w, int window_size_h,
	const mat4 &VP, const mat4 &ViewMatrix,
	const vec3 &lightInvDir,
	const Mesh &mesh_axis) const {

	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glViewport(0, 0, window_size_w, window_size_h); // Render on the whole framebuffer, complete from the lower left corner to the upper right

	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK); // Cull back-facing triangles -> draw only front-facing triangles

	// Clear the screen
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our shader
	glUseProgram(programID);

	glUniform3f(lightInvDirID, lightInvDir.x, lightInvDir.y, lightInvDir.z);

	// Send our transformation to the currently bound shader,
	// in the "MVP" uniform
	glUniformMatrix4fv(ViewMatrixID, 1, GL_FALSE, &ViewMatrix[0][0]);

	// draw the x-y-z axis
	{
		// x axis
		glUniform3f(MaterialSpecularID, 1.f, 0.f, 0.f);
		glUniform3f(MaterialAmbientID, 1.f * 0.2f, 0.f, 0.f);
		glUniform3f(MaterialDiffuseID, 1.f, 0.f, 0.f);

		mat4 ModelMatrix = rotate(-90.f, 0.f, 0.f, 1.f) * scale(0.5f, 0.5f, 0.5f);
		mat4 MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_axis.vertexbuffer, mesh_axis.normalbuffer, mesh_axis.elementbuffer, mesh_axis.indices);

		// y axis
		glUniform3f(MaterialSpecularID, 0.f, 1.f, 0.f);
		glUniform3f(MaterialAmbientID, 0.f, 1.f * 0.2f, 0.f);
		glUniform3f(MaterialDiffuseID, 0.f, 1.f, 0.f);

		ModelMatrix = scale(0.5f, 0.5f, 0.5f);
		MVP = VP * ModelMatrix;

		glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
		glUniformMatrix4fv(ModelMatrixID, 1, GL_FALSE, &ModelMatrix[0][0]);

		DrawGeometry(mesh_axis.vertexbuffer, mesh_axis.normalbuffer, mesh_axis.elementbuffer, mesh_axis.indices);
	}
}

void GLSLProgram::CleanUp() {
	glDeleteProgram(programID);
}