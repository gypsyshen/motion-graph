/*
Reference: http://www.gamedev.net/page/resources/_/technical/game-programming/bvh-file-loading-and-displaying-r3295
*/

#ifndef BVH_H
#define BVH_H

#include "Common.h"

class Bvh
{
	JOINT* loadJoint(std::istream& stream, JOINT* parent = NULL);
	void loadHierarchy(std::istream& stream);
	void loadMotion(std::istream& stream);

public:
	Bvh() {}
	~Bvh() {}

	// loading 
	void load(const std::string& filename);

	/** Loads motion data from a frame into local matrices */
	void moveTo(unsigned frame);

	const JOINT* getRootJoint() const { return rootJoint; }
	unsigned getNumFrames() const { return motionData.num_frames; }
	unsigned getNumMotionChannels() const { return motionData.num_motion_channels; }
	const MOTION& getMotion() const { return motionData; }

	OFFSET getRootPosition(unsigned frame);
	
	void concatenateMotion(MOTION newMotion);

	void translateRootJoints(OFFSET t);
		
private:
	JOINT* rootJoint;
	MOTION motionData;
};

#endif