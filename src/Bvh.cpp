#include "Bvh.h"
#include <fstream>
#include <algorithm>
#include <sstream>


static string trim(string& str) {
	remove(str.begin(), str.end(), ' ');
	return str;
}


/**
Calculates JOINT's local transformation matrix for
specified frame starting index
*/
static void moveJoint(JOINT* joint, MOTION* motionData, int frame_starts_index)
{
	// we'll need index of motion data's array with start of this specific joint
	int start_index = frame_starts_index + joint->channel_start;

	// translate indetity matrix to this joint's offset parameters
	joint->matrix = glm::translate( glm::mat4(1.0),
		glm::vec3(joint->offset.x, joint->offset.y, joint->offset.z) );

	// here we transform joint's local matrix with each specified channel's values
	// which are read from motion data
	for (int i = 0; i < joint->num_channels; i++)
	{
		// channel alias
		const short& channel = joint->channels_order[i];

		// extract value from motion data
		float value = motionData->data[start_index + i];

		if (channel & Xposition)
		{
			joint->matrix = glm::translate(joint->matrix, glm::vec3(value, 0, 0));
		}
		if (channel & Yposition)
		{
			joint->matrix = glm::translate(joint->matrix, glm::vec3(0, value, 0));
		}
		if (channel & Zposition)
		{
			joint->matrix = glm::translate(joint->matrix, glm::vec3(0, 0, value));
		}

		if (channel & Xrotation)
		{
			joint->matrix = glm::rotate(joint->matrix, value, glm::vec3(1, 0, 0));
		}
		if (channel & Yrotation)
		{
			joint->matrix = glm::rotate(joint->matrix, value, glm::vec3(0, 1, 0));
		}
		if (channel & Zrotation)
		{
			joint->matrix = glm::rotate(joint->matrix, value, glm::vec3(0, 0, 1));
		}
	}

	// then we apply parent's local transfomation matrix to this joint's LTM (local tr. mtx. :)
	if (joint->parent != NULL)
		joint->matrix = joint->parent->matrix * joint->matrix;

	// when we have calculated parent's matrix do the same to all children
	for (auto& child : joint->children)
		moveJoint(child, motionData, frame_starts_index);
}

void Bvh::moveTo(unsigned frame)
{
	// we calculate motion data's array start index for a frame
	unsigned start_index = frame * motionData.num_motion_channels;

	// recursively transform skeleton
	moveJoint(rootJoint, &motionData, start_index);
}

// loading

void Bvh::load(const std::string& filename) {

	std::fstream file;
	file.open(filename.c_str(), std::ios_base::in);

	if (file.is_open())
	{
		std::string line;

		while (file.good())
		{
			file >> line;
			if (trim(line) == "HIERARCHY") {
//				cout << "HIERARCHY" << endl;
				loadHierarchy(file);
			}
			break;
		}

		file.close();
	}
}

void Bvh::loadHierarchy(std::istream& stream) {

	std::string tmp;

	while (stream.good())
	{
		stream >> tmp;

		if (trim(tmp) == "ROOT") {
//			cout << "ROOT" << endl;
			rootJoint = loadJoint(stream);
		}
		else if (trim(tmp) == "MOTION") {
//			cout << "MOTION" << endl;
			loadMotion(stream);
		}
	}
}

JOINT* Bvh::loadJoint(std::istream& stream, JOINT* parent)
{
	JOINT* joint = new JOINT;
	joint->parent = parent;

	// load joint name
	std::string* name = new std::string;
	stream >> *name;
	joint->name = name->c_str();

//	cout << "JOINT " << joint->name << endl;

	std::string tmp;

	// setting local matrix to identity
	joint->matrix = glm::mat4(1.0);

	static int _channel_start = 0;
	unsigned channel_order_index = 0;

	while (stream.good())
	{
		stream >> tmp;
		tmp = trim(tmp);

		// loading channels
		char c = tmp.at(0);
		if (c == 'X' || c == 'Y' || c == 'Z')
		{
			if (tmp == "Xposition")
			{
				joint->channels_order[channel_order_index++] = Xposition;
			}
			if (tmp == "Yposition")
			{
				joint->channels_order[channel_order_index++] = Yposition;
			}
			if (tmp == "Zposition")
			{
				joint->channels_order[channel_order_index++] = Zposition;
			}

			if (tmp == "Xrotation")
			{
				joint->channels_order[channel_order_index++] = Xrotation;
			}
			if (tmp == "Yrotation")
			{
				joint->channels_order[channel_order_index++] = Yrotation;
			}
			if (tmp == "Zrotation")
			{
				joint->channels_order[channel_order_index++] = Zrotation;
			}
		}

		if (tmp == "OFFSET")
		{
			// reading an offset values
			stream >> joint->offset.x
				>> joint->offset.y
				>> joint->offset.z;

//			cout << "OFFSET " << joint->offset.x << " " << joint->offset.y << " " << joint->offset.z << endl;
		}
		else if (tmp == "CHANNELS")
		{
			// loading num of channels
			stream >> joint->num_channels;

			// adding to motiondata
			motionData.num_motion_channels += joint->num_channels;

			// increasing static counter of channel index starting motion section
			joint->channel_start = _channel_start;
			_channel_start += joint->num_channels;

			// creating array for channel order specification
			joint->channels_order = new short[joint->num_channels];

//			cout << "CHANNELS " << joint->num_channels << endl;
		}
		else if (tmp == "JOINT")
		{
			// loading child joint and setting this as a parent
			JOINT* tmp_joint = loadJoint(stream, joint);

			tmp_joint->parent = joint;
			joint->children.push_back(tmp_joint);
		}
		else if (tmp == "End")
		{
			// loading End Site joint
			stream >> tmp >> tmp; // Site {

			JOINT* tmp_joint = new JOINT;

			tmp_joint->parent = joint;
			tmp_joint->num_channels = 0;
			tmp_joint->name = "EndSite";
			joint->children.push_back(tmp_joint);

//			cout << tmp_joint->name << endl;

			stream >> tmp;
			if (tmp == "OFFSET") {
				stream >> tmp_joint->offset.x
					>> tmp_joint->offset.y
					>> tmp_joint->offset.z;
//				cout << "OFFSET " << tmp_joint->offset.x << " " << tmp_joint->offset.y << " " << tmp_joint->offset.z << endl;
			}

			stream >> tmp;
		}
		else if (tmp == "}")
			return joint;

	}
}

void Bvh::loadMotion(std::istream& stream)
{
	std::string tmp;

	while (stream.good())
	{
		stream >> tmp;

		if (trim(tmp) == "Frames:")
		{
			// loading frame number
			stream >> motionData.num_frames;
		}
		else if (trim(tmp) == "Frame")
		{
			// loading frame time
			float frame_time;
			stream >> tmp >> frame_time;

			int num_frames = motionData.num_frames;
			int num_channels = motionData.num_motion_channels;

//			cout << "num_frames: " << num_frames << endl;
//			cout << "num_channels: " << num_channels << endl;

			// creating motion data array
			motionData.data = new float[num_frames * num_channels];
			
			// foreach frame read and store floats
			for (int frame = 0; frame < num_frames; frame++)
			{
				for (int channel = 0; channel < num_channels; channel++)
				{
					// reading float
					float x;
					std::stringstream ss;
					stream >> tmp;
					ss << tmp;
					ss >> x;

					// calculating index for storage
					int index = frame * num_channels + channel;
					motionData.data[index] = x;
//					motionData.data.push_back(x);
				}
			}
		}
	}
}


void Bvh::concatenateMotion(MOTION newMotion) {
	
//	motionData.num_frames += newMotion.num_frames;
//	motionData.data.insert(motionData.data.end(), newMotion.data.begin(), newMotion.data.end());

	unsigned num_total_frames = motionData.num_frames + newMotion.num_frames;
	float * data = new float[num_total_frames * motionData.num_motion_channels];
	int index = 0;
	for (unsigned frame = 0; frame < motionData.num_frames; ++frame) {
		for (unsigned i = 0; i < motionData.num_motion_channels; ++i) {
			data[index] = motionData.data[frame * motionData.num_motion_channels + i];
			index++;
		}
	}
	for (unsigned frame = 0; frame < newMotion.num_frames; ++frame) {
		for (unsigned i = 0; i < newMotion.num_motion_channels; ++i) {
			data[index] = newMotion.data[frame * newMotion.num_motion_channels + i];
			index++;
		}
	}

	delete motionData.data;
	motionData.data = data;

	motionData.num_frames = num_total_frames;
}


void Bvh::translateRootJoints(OFFSET t) {

	unsigned num_frames = motionData.num_frames; // this is a hack, ignore the last frame to eliminate
	unsigned num_motion_channels = motionData.num_motion_channels;

	// translate the root joint of every frame by t
	for (unsigned frame = 0; frame < num_frames; ++frame) {

		// move to the current frame
		moveTo(frame);

		// start index of current frame
		int frame_start_index = frame * num_motion_channels;
		// start index of the root joint
		int start_index = frame_start_index;

		for (int i = 0; i < rootJoint->num_channels; ++i) {

			const short& channel = rootJoint->channels_order[i];

			float value = motionData.data[start_index + i];

			if (channel & Xposition) value += t.x;
			if (channel & Yposition) value += t.y;
			if (channel & Zposition) value += t.z;

			motionData.data[start_index + i] = value;
		}

	}
}

OFFSET Bvh::getRootPosition(unsigned frame) {

	moveTo(frame);

	unsigned start_index = frame * motionData.num_motion_channels + rootJoint->channel_start;

	OFFSET position;

	for (unsigned i = 0; i < rootJoint->num_channels; ++i) {

		const short & channel = rootJoint->channels_order[i];
		if (channel & Xposition) position.x = motionData.data[start_index + i];
		if (channel & Yposition) position.y = motionData.data[start_index + i];
		if (channel & Zposition) position.z = motionData.data[start_index + i];
	}

	return position;
}
