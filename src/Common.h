/*
Structures for storing data of BVH file format.
Reference: http://www.gamedev.net/page/resources/_/technical/game-programming/bvh-file-loading-and-displaying-r3295
*/

#ifndef COMMON_H
#define COMMON_H

#include "Header.h"

#define Xposition 0x01
#define Yposition 0x02
#define Zposition 0x04
#define Zrotation 0x10
#define Xrotation 0x20
#define Yrotation 0x40


typedef struct OFFSET OFFSET;
typedef struct OFFSET VECTOR;
typedef struct OFFSET POINT;

struct OFFSET{

	float x, y, z;

	OFFSET(float _x, float _y, float _z) : x(_x), y(_y), z(_z) {}
	OFFSET(): x(0.f), y(0.f), z(0.f) {}
};

typedef struct JOINT JOINT;

struct JOINT {

	const char* name = NULL;        // joint name
	JOINT* parent = NULL;           // joint parent
	OFFSET offset;                  // offset data
	unsigned int num_channels = 0;  // num of channels joint has
	short* channels_order = NULL;   // ordered list of channels
	std::vector<JOINT*> children;   // joint's children
	glm::mat4 matrix;               // local transofrmation matrix (premultiplied with parents')
	unsigned int channel_start = 0; // index of joint's channel data in motion array

	VECTOR velocity = VECTOR();		// velocity
	VECTOR acceleration = VECTOR();	// acceleration
	VECTOR net_torque = VECTOR();	// net torque
	VECTOR force = VECTOR();		// force

	float torque = 0.f;				// torque
};

typedef struct {

	JOINT* rootJoint;
	int num_channels;
} HIERARCHY;

typedef struct MOTION {

	unsigned int num_frames;              // number of frames
	unsigned int num_motion_channels; // number of motion channels 
	float* data;                   // motion float data array
//	vector<float> data;                   // motion float data array
	unsigned* joint_channel_offsets;      // number of channels from beggining of hierarchy for i-th joint

	MOTION() : num_motion_channels(0), data(NULL) {}

} MOTION;

#endif